package com.example.orders.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order-service")
public class OrderController {

	@GetMapping("/{userId}/orders")
	public ResponseEntity<String> getOrders(@PathVariable String userId){
		return ResponseEntity.status(HttpStatus.OK).body(userId+" 주문정보입니다.");
	}
}
